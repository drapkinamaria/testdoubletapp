from django.contrib import admin
from .models import AdminUser

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"

@admin.register(AdminUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ['age']