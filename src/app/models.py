from django.db import models
from django.contrib.auth.models import AbstractUser

class AdminUser(AbstractUser):
    age = models.PositiveIntegerField(null=True, blank=True)

class TelegramUser(models.Model):
    user_id = models.CharField(max_length=100, unique=True)
    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    username = models.CharField(max_length=100, null=True, blank=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.username or "Unknown User"