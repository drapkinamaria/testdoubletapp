from aiogram import Dispatcher, types

async def echo_message(message: types.Message):
    await message.answer(message.text)

def register_handlers_common(dp: Dispatcher):
    dp.register_message_handler(echo_message)
