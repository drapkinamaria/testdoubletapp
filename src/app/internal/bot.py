from aiogram import Bot, Dispatcher, executor, types
from src.config.settings import TELEGRAM_BOT_TOKEN, BASE_DIR
import aiosqlite
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.contrib.fsm_storage.memory import MemoryStorage
import logging
from aiohttp import web

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

bot = Bot(token=TELEGRAM_BOT_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
dp.middleware.setup(LoggingMiddleware())

DATABASE_PATH = BASE_DIR / "db.sqlite3"

class Form(StatesGroup):
    phone = State()
    phone_set = State()

async def db_create():
    async with aiosqlite.connect(DATABASE_PATH) as db:
        await db.execute("""CREATE TABLE IF NOT EXISTS users
                            (id INTEGER PRIMARY KEY,
                             username TEXT,
                             full_name TEXT,
                             phone TEXT)""")
        await db.commit()

@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    user_id = message.from_user.id
    username = message.from_user.username
    full_name = message.from_user.full_name
    try:
        async with aiosqlite.connect(DATABASE_PATH) as db:
            await db.execute("INSERT INTO users (id, username, full_name) VALUES (?, ?, ?)",
                             (user_id, username, full_name))
            await db.commit()
        await message.reply("Добро пожаловать! Чтобы использовать все функции бота, пожалуйста, зарегистрируйте свой "
                            "номер телефона с помощью команды /set_phone.")
    except Exception as e:
        logger.exception("Ошибка при работе с базой данных")
        await message.reply("Произошла ошибка, пожалуйста, попробуйте еще раз.")


@dp.message_handler(commands=['set_phone'], state='*')
async def set_phone(message: types.Message, state: FSMContext):
    await Form.phone.set()
    logger.info(f"State set to 'phone'. Asking user {message.from_user.id} for phone number.")
    await message.reply("Пожалуйста, отправьте ваш номер телефона.")


@dp.message_handler(state=Form.phone)
async def process_phone(message: types.Message, state: FSMContext):
    logger.info(f"Received phone number from user {message.from_user.id}: {message.text}")
    phone_number = message.text.strip()

    user_id = message.from_user.id
    username = message.from_user.username
    full_name = message.from_user.full_name

    try:
        async with aiosqlite.connect(DATABASE_PATH) as db:
            cursor = await db.execute("SELECT id FROM users WHERE id = ?", (user_id,))
            user = await cursor.fetchone()
            if user:
                await db.execute("UPDATE users SET phone = ?, username = ?, full_name = ? WHERE id = ?",
                                 (phone_number, username, full_name, user_id))
                await db.commit()
                logger.info(f"Phone number for user {user_id} updated to {phone_number}.")
                await message.reply("Ваш номер телефона сохранён!")
            else:
                logger.warning(f"No user found with ID {user_id}. Cannot update phone number.")
                await message.reply("Не найден пользователь для обновления номера телефона.")
    except Exception as e:
        logger.exception("Ошибка при работе с базой данных: %s", str(e))
        await message.reply("Произошла ошибка при сохранении номера телефона.")
    finally:
        await state.finish()

    await Form.phone_set.set()

@dp.message_handler(commands=['me'], state=Form.phone_set)
async def user_info(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    username = message.from_user.username
    full_name = message.from_user.full_name


    response = (f"ID пользователя: {user_id}\n"
                f"Имя пользователя: @{username}\n"
                f"Полное имя: {full_name}\n"
                )

    await message.reply(response)

@dp.message_handler(commands=['me'], state='*')
async def user_info_not_set(message: types.Message, state: FSMContext):
    await message.reply("Ваш номер телефона не зарегистрирован. "
                        "Пожалуйста, зарегистрируйте его с помощью команды /set_phone.")


@dp.message_handler(lambda message: not message.text.startswith(('/set_phone', '/start')), state=None)
async def all_other_messages(message: types.Message):
    async with aiosqlite.connect(DATABASE_PATH) as db:
        cursor = await db.execute("SELECT phone FROM users WHERE id = ?", (message.from_user.id,))
        row = await cursor.fetchone()
        if row and row[0]:
            return
    await message.reply("Пожалуйста, сначала зарегистрируйте свой номер телефона с помощью команды /set_phone.")

async def me_endpoint(request):
    return web.json_response({'message': 'This is the /me endpoint'})

app = web.Application()
app.router.add_get('/me', me_endpoint)


async def on_startup(_):
    logger.info("Starting bot and web server...")
    await db_create()

    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, 'localhost', 8080)
    await site.start()

if __name__ == '__main__':
    executor.start_polling(dp, on_startup=on_startup)
